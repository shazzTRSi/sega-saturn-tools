/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.trsi.consoledivision.saturn;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.util.Arrays.asList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.tabbedui.VerticalLayout;

/**
 *
 * @author SC2
 */
public final class CurveExporter extends ApplicationFrame implements ActionListener, ChartMouseListener {

    private static XYLineAndShapeRenderer localXYLineAndShapeRenderer;
    private XYSeries series;
    private String output;
            
    public CurveExporter(String paramString, int nbPoints, String output) 
    {
        super(paramString);
        this.output = output;
        
        JPanel mainPanel = new JPanel(new VerticalLayout());

        JPanel chartPanel = createCurvePanel(nbPoints, 2048, 2048, 4, 5, Math.PI);
        chartPanel.setPreferredSize(new Dimension(1000, 700));
                
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setBackground(Color.white);
        JButton exportButton = new JButton("Export points");
        exportButton.setActionCommand("Export");
        exportButton.addActionListener(this);
        buttonPanel.add(exportButton);

        mainPanel.add(chartPanel);
        mainPanel.add(buttonPanel);
        mainPanel.setPreferredSize(new Dimension(1100, 800));
        setContentPane(mainPanel);
    }

    public void exportData()
    {
        try {
            BufferedWriter bufferedWriter = null;
            Path path_out = Paths.get(output);
            
            bufferedWriter = Files.newBufferedWriter(path_out, Charset.forName("UTF-8"));
            String prefix = path_out.getFileName().toString().substring(0, path_out.getFileName().toString().lastIndexOf('.')).replace(' ', '_');
                       
            StringBuilder sb = new StringBuilder();
            sb.append("const uint16_t " + prefix + "_coord_x[" + series.getItemCount() + "] __attribute__((aligned(4))) = {\n\t//Coord X\n\t");
            for (int i = 1; i <= series.getItemCount(); i++)
            {
                sb.append((long)series.getX(i-1).doubleValue());
                if (i != series.getItemCount()) sb.append(", ");
                if(i % 16 == 0) sb.append("\n\t");
            }         
            sb.append("\n};\n\n");
            sb.append("const uint16_t " + prefix + "_coord_y[" + series.getItemCount() + "] __attribute__((aligned(4))) = {\n\t//Coord Y\n\t");
            for (int i = 1; i <= series.getItemCount(); i++)
            {
                sb.append((long)series.getY(i-1).doubleValue());
                if (i != series.getItemCount()) sb.append(", ");
                if(i % 16 == 0) sb.append("\n\t");
            }         
            sb.append("\n};\n\n");            
                           
            bufferedWriter.write(sb.toString(), 0, sb.length());
            bufferedWriter.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(CurveExporter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public JPanel createCurvePanel(int nbPoints, int amplitudeX, int amplitudeY, int alpha, int beta, double theta) {

        series = new XYSeries("Lissajous", false);

        for (double t = 0; t <= 2 * Math.PI; t += (2 * Math.PI / nbPoints)) 
        {
            double x, y;
            x = amplitudeX * Math.sin(alpha * t + theta);
            y = amplitudeY * Math.sin(beta * t);
            series.add((int)x, (int)y);
        }

        JFreeChart localJFreeChart = createParametricChart(series);
        ChartPanel localChartPanel = new ChartPanel(localJFreeChart);
        localChartPanel.addChartMouseListener(this);
        localChartPanel.setMouseWheelEnabled(true);

        return localChartPanel;
    }

    private static JFreeChart createParametricChart(XYSeries series) {
        JFreeChart localJFreeChart = ChartFactory.createXYLineChart("Curve Renderer", "x", "y", new XYSeriesCollection(series), PlotOrientation.VERTICAL, true, true, true);

        XYPlot localXYPlot = (XYPlot) localJFreeChart.getPlot();
        localXYPlot.setDomainZeroBaselineVisible(true);
        localXYPlot.setRangeZeroBaselineVisible(true);
        localXYPlot.getDomainAxis().setLowerMargin(0.0D);
        localXYPlot.getDomainAxis().setUpperMargin(0.0D);
        localXYPlot.setDomainPannable(true);
        localXYPlot.setRangePannable(true);
        localXYPlot.setAxisOffset(new RectangleInsets(4.0D, 4.0D, 4.0D, 4.0D));
        localXYPlot.setBackgroundPaint(Color.WHITE);
        localXYPlot.setDomainGridlinePaint(Color.LIGHT_GRAY);
        localXYPlot.setRangeGridlinePaint(Color.LIGHT_GRAY);

        localXYLineAndShapeRenderer = (XYLineAndShapeRenderer) localXYPlot.getRenderer();
        localXYLineAndShapeRenderer.setSeriesShapesVisible(0, true);
        localXYLineAndShapeRenderer.setSeriesShape(0, new Rectangle(3, 3));
        localXYLineAndShapeRenderer.setLegendLine(new Rectangle2D.Double(-4.0D, -3.0D, 8.0D, 6.0D));

        return localJFreeChart;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        OptionParser p = new OptionParser();
        p.allowsUnrecognizedOptions();
        p.acceptsAll(asList("h", "?"), "show this help").forHelp();

        p.acceptsAll(asList("points", "p"), "output file full path").withRequiredArg().ofType(Integer.class).defaultsTo(255);
        p.acceptsAll(asList("output", "o"), "output file full path").withRequiredArg().required();

        OptionSet opt = p.parse(args);
        Integer points = (Integer) opt.valueOf("points");
        String output = (String) opt.valueOf("output");

        CurveExporter ce = new CurveExporter("TRSi Curve Exporter", points, output);
           
        ce.pack();
        RefineryUtilities.centerFrameOnScreen(ce);
        ce.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        exportData();
    }

    @Override
    public void chartMouseClicked(ChartMouseEvent cme) {

    }

    @Override
    public void chartMouseMoved(ChartMouseEvent cme) {
        ChartEntity localChartEntity = cme.getEntity();
        if ((localChartEntity instanceof XYItemEntity)) 
        {
            localXYLineAndShapeRenderer.setBaseItemLabelGenerator(new CustomXYItemLabelGenerator(((XYItemEntity)localChartEntity).getItem()));
            localXYLineAndShapeRenderer.setBaseItemLabelsVisible(true);            
        }
        else
        {
            localXYLineAndShapeRenderer.setBaseItemLabelsVisible(false);   
        }
   }

    private static class CustomXYItemLabelGenerator implements XYItemLabelGenerator 
    {
        private final int item;

        public CustomXYItemLabelGenerator(int item) 
        {
            this.item = item;
        }

        @Override
        public String generateLabel(XYDataset xyd, int series, int item) 
        {
            String str = "";
            if(this.item == item)
            {
                str = "item " + item + " (" + String.valueOf((int)xyd.getXValue(series, item)) + ", " + String.valueOf((int)xyd.getYValue(series, item)) + ")";
            }
            
            return str;
        }
    }
}
