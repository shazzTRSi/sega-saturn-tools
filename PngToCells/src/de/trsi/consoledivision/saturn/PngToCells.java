/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.trsi.consoledivision.saturn;

import ar.com.hjg.pngj.IImageLineSet;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.chunks.PngChunkPLTE;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 *
 * @author SC2
 */
public class PngToCells
{
    private static final Mode MODE_1PN_16C_NOAUX = new Mode(16, (1<<10), "PN: 1 word  - CP: 16 colors, aux=0", 1);
    private static final Mode MODE_1PN_16C_AUX = new Mode(16, (1<<12), "PN: 1 word  - CP: 16 colors, aux=1", 1);
    private static final Mode MODE_1PN_256C_NOAUX = new Mode(26, (1<<10)/2, "PN: 1 word  - CP: 256 colors, aux=0", 1);
    private static final Mode MODE_1PN_256C_AUX = new Mode(256, (1<<12)/2, "PN: 1 word  - CP: 256 colors, aux=1", 1);
    private static final Mode MODE_2PN_16C = new Mode(16, (1<<15), "PN: 2 words - CP: 256 colors", 2); 
    private static final Mode MODE_2PN_256C = new Mode(256, (1<<15)/2, "PN: 2 words - CP: 256 colors", 2); 
    
    static final List<Mode> MODES = new ArrayList<Mode>() {{   
        add(MODE_1PN_16C_NOAUX);
        add(MODE_1PN_16C_AUX);
        add(MODE_1PN_256C_NOAUX);
        add(MODE_1PN_256C_AUX);     
        add(MODE_2PN_16C);   
        add(MODE_2PN_256C); 
    }};

    private final HashMap<Integer, Integer> palette;
    private IImageLineSet<ImageLineInt> scanlines;
    private int palsize;
    private final CRC32 crcEngine;
    private HashMap<Integer, Cell> orderedBlocks = new HashMap<>();
    private HashMap<Integer, Cell> filteredBlocks = new HashMap<>();  
    private final List[] pages;
    private int width;
    private int height;
    private String prefix;
    private int pnformat;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            PngToCells pngToCells = new PngToCells();

            OptionParser p = new OptionParser();
            p.allowsUnrecognizedOptions();
            p.acceptsAll(asList("h", "?"), "show this help").forHelp();

            p.acceptsAll(asList("input", "i"), "input file full path").withRequiredArg().required();
            p.acceptsAll(asList("output", "o"), "output file full path").withRequiredArg().required();
            p.acceptsAll(asList("format", "f"), "1 or 2 word PN format").withRequiredArg().ofType(Integer.class).defaultsTo(1);

            OptionSet opt = p.parse(args);
            String input = (String) opt.valueOf("input");
            String output = (String) opt.valueOf("output");
            Integer format = (Integer) opt.valueOf("format");

            pngToCells.process(input, output, format);
            
            SanityCheck sn = new SanityCheck(output, pngToCells.prefix);
            sn.generatePages();
        } 
        catch (PngToCellsException | IOException ex)
        {
            Logger.getLogger(PngToCells.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Constructor
     */
    public PngToCells()
    {
        this.palette = new HashMap();
        this.crcEngine = new CRC32();
        this.palsize = 0;
        
        this.orderedBlocks = new HashMap<>();
        this.filteredBlocks = new HashMap<>();          
        
        this.pages = new List[4];
        this.pages[0] = new ArrayList();
        this.pages[1] = new ArrayList();
        this.pages[2] = new ArrayList();
        this.pages[3] = new ArrayList();                
    }

    /**
     *
     * @param input
     * @param output
     * @param format
     * @throws PngToCellsException
     * @throws IOException
     */
    public void process(String input, String output, int format) throws PngToCellsException, IOException
    {
        BufferedWriter bufferedWriter = null;
        InputStream is = null;

        try
        {
            int nbBlocks = 0;
            int nbBlocksPerLine = 0;            
            
            if (format != 1 && format != 2) throw new PngToCellsException("Unknown format : " + format);
            this.pnformat = format;
            
            Path path_in = Paths.get(input);
            if(!path_in.toFile().exists()) throw new PngToCellsException("Input file missing : " + path_in);
                
            Path path_out = Paths.get(output);
            this.prefix = path_out.getFileName().toString().substring(0, path_out.getFileName().toString().lastIndexOf('.')).replace(' ', '_');

            byte[] data = Files.readAllBytes(path_in);
            is = new ByteArrayInputStream(data);

            bufferedWriter = Files.newBufferedWriter(Paths.get(output), Charset.forName("UTF-8"));

            PngReader pngr = new PngReader(is, true);
            
            if (!pngr.getImgInfo().indexed) throw new PngToCellsException("Not a indexed PNG");

            this.height = pngr.getImgInfo().rows;
            this.width = pngr.getImgInfo().cols;       
            
            // Extract palette
            String palBuf = extractPalette(pngr.getMetadata().getPLTE());
            bufferedWriter.write(palBuf, 0, palBuf.length());

            // keep pixels
            this.scanlines = (IImageLineSet<ImageLineInt>) pngr.readRows();

            // check this is modulo 256
            if ((this.height % 256 != 0) || (this.width % 256 != 0))
            {
                throw new PngToCellsException("Image is 256 or 512px : " + width + " x " + height);
            } 
            else
            {
                nbBlocks = (this.width / 8) * (this.height / 8);
                nbBlocksPerLine = (this.width / 8);
            }
            
            extractCells(nbBlocks, nbBlocksPerLine);

            String bufCP = extractCharacterPatternTable();
            bufferedWriter.write(bufCP, 0, bufCP.length());

            String buffPNT = extractPatternNameTable();
            bufferedWriter.write(buffPNT, 0, buffPNT.length());

            is.close();
            pngr.close();
            bufferedWriter.close();
        } 
        catch (IOException ex)
        {
            Logger.getLogger(PngToCells.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally
        {
            try
            {
                if (bufferedWriter != null)
                {
                    bufferedWriter.close();
                }
                if (is != null)
                {
                    is.close();
                }
            } catch (IOException ex)
            {
                throw new PngToCellsException(ex.getMessage());
            }
        }

    }

    /*
     * Not perfect linear RGB5 to BGR5 conversion. 
     */
    private int rgb8tobgr5(int rgb8)
    {
        int r8 = (rgb8 >> 16) & 0xFF;
        int g8 = (rgb8 >> 8) & 0xFF;
        int b8 = rgb8 & 0xFF;

        int r5 = (r8 >> 3) & 0x1F;
        int g5 = (g8 >> 3) & 0x1F;
        int b5 = (b8 >> 3) & 0x1F;

        int bgr5 = ((b5 << 10) | (g5 << 5) | r5) & 0x7FFF;
        return bgr5;

    }
    
    /*
     * void extractCells(int nbBlocks, int nbBlocksPerLine) throws PngToCellsException
     */
    private void extractCells(int nbBlocks, int nbBlocksPerLine) throws PngToCellsException
    {
        int blkNb = 0;
        int planeX = 0;
        int planeY = 0;

        HashMap<Long, Integer> crcBlocks = new HashMap<>();
        
        // divide in 8x8 blocks
        for (int n = 0; n < nbBlocks; n++)
        {
            int firstRow = (n / nbBlocksPerLine) * 8;
            int firstCol = (n % nbBlocksPerLine) * 8;

            this.crcEngine.reset();

            Cell block = new Cell(this.palsize);

            // read the lines
            for (int y = 0; y < 8; y++)
            {
                ImageLineInt aScanline = (ImageLineInt) scanlines.getImageLine(firstRow + y);
                for (int x = 0; x < 8; x++)
                {
                    int px = aScanline.getElem(x + firstCol);
                    if (this.palsize == 16 || this.palsize == 256)
                    {
                        if (px > this.palsize)
                        {
                            throw new PngToCellsException("Palette offset > " + this.palsize + " : " + px);
                        }
                        block.data[x + (y * 8)] = (short) px;
                    } else
                    {
                        throw new PngToCellsException("Unknown pal size : " + this.palsize);
                    }
                }
            }

            // CRC engine needs a table of byte, as we store unsigned byte in short[] to avoid sign issue, let's create quickly a table of bytes....
            // even if then byte are signed, the CRC computation is still ok
            byte[] tmp = new byte[block.data.length];
            for (int i = 0; i < block.data.length; i++)
            {
                tmp[i] = (byte) (block.data[i] & 0xFF);
            }

            // add the signature and the data to the block
            this.crcEngine.update(tmp);
            block.number = blkNb;
            block.lineNb = firstRow;

            if (planeX < 512 && planeY < 512)
            {
                this.pages[0].add(n);
            } 
            else if (planeX >= 512 && planeY < 512)
            {
                this.pages[1].add(n);
            } 
            else if (planeX < 512 && planeY >= 512)
            {
                this.pages[2].add(n);
            } 
            else if (planeX >= 512 && planeY >= 512)
            {
                this.pages[3].add(n);
            } 
            else
            {
                throw new PngToCellsException("outside 4 planes : " + planeX + " / " + planeY);
            }

            planeX = (planeX + 8);
            if (planeX == this.width)
            {
                planeY = (planeY + 8);
                planeX = 0;
            }

            // check if we already see this block sooner given its signature
            if (!crcBlocks.containsKey(this.crcEngine.getValue()))
            {
                // no ? so let's add it to the sorted list and filtered list, and we increment the unique block number
                crcBlocks.put(this.crcEngine.getValue(), n);
                this.orderedBlocks.put(n, block);
                this.filteredBlocks.put(blkNb, block);
                blkNb++;
            } 
            else
            {
                // yes, so we only add this block to the ordered list but we change the cell to the first seen one
                Cell cell = this.orderedBlocks.get(crcBlocks.get(this.crcEngine.getValue()));
                this.orderedBlocks.put(n, cell);
            }
        }
        
        System.out.println("The following modes can be used:");
        Mode minMode = (this.palsize==16)?MODE_2PN_16C:MODE_2PN_256C;
        for(Mode mode : MODES)
        {
            if(this.filteredBlocks.size() <= mode.maxPatterns && this.palsize == mode.colors)
            {
                if(mode.sizePN == 1) minMode = mode;
                System.out.println(mode.desc + " (" + this.filteredBlocks.size() + "/" + mode.maxPatterns + ")");
            }
        }
        if(this.pnformat == 1 && minMode.sizePN == 2)
        {
            Logger.getLogger(PngToCells.class.getName()).log(Level.WARNING, "Forcing 2 words PN");
            this.pnformat = 2;
        }
          
        Logger.getLogger(PngToCells.class.getName()).log(Level.INFO, "{0} cells on {1} are unique ", new Object[]
        {
            this.filteredBlocks.size(), this.orderedBlocks.size()
        });    
   
    }
    
    private String extractCharacterPatternTable() throws PngToCellsException
    {
        StringBuilder sb = new StringBuilder();
        if (this.palsize == 16)
        {
            sb.append("const uint32_t " + this.prefix + "_character_pattern[").append(this.filteredBlocks.size() * 8).append("] __attribute__((aligned(4))) = { //").append(this.filteredBlocks.size()).append(" cells instead of ").append(this.orderedBlocks.size()).append("\n\t");
            sb.append("// each line is a 8x8 cell. each byte is 2x4bits color index in the cell palette, 4 bytes is 4 color indexes\n\t");
        } 
        else if (this.palsize == 256)
        {
            sb.append("const uint32_t " + prefix + "_character_pattern[").append(this.filteredBlocks.size() * 16).append("] __attribute__((aligned(4))) = { //").append(this.filteredBlocks.size()).append(" cells instead of ").append(this.orderedBlocks.size()).append("\n\t");
            sb.append("// each line is a 8x8 cell. each byte is 1 color index in the cell palette, 4 bytes is 4 color indexes\n\t");
        } 
        else
        {
            throw new PngToCellsException(("Non supported palette size"));
        }

        // generate pattern data only based on unique blocks
        for (int n = 0; n < this.filteredBlocks.size(); n++)
        {
            Cell aCell = this.filteredBlocks.get(n);
            int val;

            if (this.palsize == 16)
            {
                for (int l = 0; l < 8; l++) // in color 16 mode, each 8x8 block consists in 8 32bits data (32 bytes).
                {
                    // to generate uint32_t entries
                    int dot1 = aCell.data[(l * 8) + 0];
                    int dot2 = aCell.data[(l * 8) + 1];
                    int dot3 = aCell.data[(l * 8) + 2];
                    int dot4 = aCell.data[(l * 8) + 3];
                    int dot5 = aCell.data[(l * 8) + 4];
                    int dot6 = aCell.data[(l * 8) + 5];
                    int dot7 = aCell.data[(l * 8) + 6];
                    int dot8 = aCell.data[(l * 8) + 7];

                    val = (dot1 << 28) | (dot2 << 24) | (dot3 << 20) | (dot4 << 16) | (dot5 << 12) | (dot6 << 8) | (dot7 << 4) | dot8;
                    sb.append(toHexString(val, 8));
                    sb.append(", ");
                }
            } 
            else if (this.palsize == 256) // in color 256 mode, each 8x8 block consists in 16 32bits data (64 bytes).
            {
                for (int l = 0; l < 16; l++)
                {
                    int dot1 = aCell.data[(l * 4) + 0];
                    int dot2 = aCell.data[(l * 4) + 1];
                    int dot3 = aCell.data[(l * 4) + 2];
                    int dot4 = aCell.data[(l * 4) + 3];

                    val = (dot1 << 24) | (dot2 << 16) | (dot3 << 8) | dot4;
                    sb.append(toHexString(val, 8));
                    sb.append(", ");
                }
            }
            sb.append("// line ")
                    .append(aCell.lineNb / 8)
                    .append(" - cell ")
                    .append(aCell.number)
                    .append(" (")
                    .append(8 * aCell.number % (this.width / 8)) // x
                    .append(",")
                    .append(aCell.lineNb) // y
                    .append(") ")
                    .append(" (")
                    .append(toHexString(aCell.number, 4))
                    .append(")\n\t");
        }
        sb.append("\n};\n\n");      
        
        return sb.toString();
    }

    private String extractPatternNameTable()
    {
        
        // create the pattern name table (map of patterns)
        // to do this we just browse the ordered list
        // to avoid code on the saturn, we can patch the pointer based on palette CRAM and Pattern VRAM locations
        //      uint16_t _nbg2_cell_data_number = VDP2_PN_CONFIG_1_CHARACTER_NUMBER((uint32_t)VRAM_ADDR_4MBIT(2, 0x1000));
        //      uint16_t _nbg2_palette_number = VDP2_PN_CONFIG_0_PALETTE_NUMBER(CRAM_MODE_0_OFFSET(NBG2_VRAM_PAL_NB, 0, 0));
        // 	uint16_t cell_data_number = _nbg2_cell_data_number + pattern_name_table[i];
        //	nbg2_page0[i] = cell_data_number | _nbg2_palette_number;
        //         

        StringBuilder sb = new StringBuilder();
        int tab = 0;
        for (int p = 0; p < this.pages.length; p++)
        {
            if (this.pages[p].size() > 0)
            {
                sb.append("\nuint" + this.pnformat*16 + "_t " + prefix + "_pattern_name_table_page_" + p + "[").append(pages[p].size()).append("] __attribute__((aligned(4))) = {\n\t");
                sb.append("// each line consists of 64 " + ((this.pnformat==2)?"long words":"words") + " pointers (8x64=512px) to the cell data, one page is 64x64 (512x512px) " + ((this.pnformat==2)?"long words":"words") + " pointers\n\t");
                for (int n = 0; n < this.pages[p].size(); n++)
                {
                    tab++;
                    Cell block = this.orderedBlocks.get((int) this.pages[p].get(n));
                    if (this.palsize == 16)  // with 0x20 boundaty each pointer address == block number as one block equals to x20 bytes
                    {
                        sb.append(toHexString(block.number, this.pnformat*4));
                    } 
                    else if (this.palsize == 256)   // with 0x20 boundaty each pointer address == 2*block number as one block equals to x40 bytes
                    {
                        sb.append(toHexString(block.number * 2, this.pnformat*4));
                    }

                    if (tab % 64 == 0)
                    {
                        sb.append(",\n\t");
                    } else
                    {
                        sb.append(", ");
                    }
                }
                sb.append("\n};\n");
            }
        }
        
        return sb.toString();
    }

    private String extractPalette(PngChunkPLTE pal) throws PngToCellsException
    {
        StringBuilder sb = new StringBuilder();

        if (pal.getNentries() <= 16)
        {
            this.palsize = 16;
            sb.append("const uint16_t " + this.prefix + "_cell_palette[").append(16).append("] __attribute__((aligned(4))) = {\n\t//16 colors palette in BGR15 format (not RGB)\n\t");
            for (int i = 0; i < pal.getNentries(); i++)
            {
                int bgr5 = rgb8tobgr5(pal.getEntry(i));

                this.palette.put(i, bgr5);
                sb.append(toHexString(bgr5, 4));
                if (i != 16 - 1)
                {
                    sb.append(", ");
                }
            }
            sb.append("\n};\n\n");
            
            return sb.toString();
        } 
        else if (pal.getNentries() == 256)
        {
            int tab = 0;
            this.palsize = 256;
            sb.append("const uint16_t " + this.prefix + "_cell_palette[").append(pal.getNentries()).append("] __attribute__((aligned(4))) = {\n\t//256 colors palette in BGR15 format (not RGB)\n\t");
            for (int i = 0; i < pal.getNentries(); i++)
            {
                tab++;
                int bgr5 = rgb8tobgr5(pal.getEntry(i));

                this.palette.put(i, bgr5);
                sb.append(toHexString(bgr5, 4));

                if (i != 256 - 1)
                {
                    if (tab % 16 == 0)
                    {
                        sb.append(",\n\t");
                    } else
                    {
                        sb.append(", ");
                    }
                }
            }
            sb.append("\n};\n\n");
            
            return sb.toString();
        } 
        else
        {
            throw new PngToCellsException("Palette is too big : " + pal.getNentries());
        }
    }

    private String toHexString(int entry, int size)
    {
        return "0x" + String.format("%0" + size + "x", entry).toUpperCase();
    } 
    
    protected static class Mode
    {
        public int colors;
        public int maxPatterns;
        public String desc;
        public int sizePN;

        public Mode(int colors, int maxPatterns, String desc, int sizePN) 
        {
            this.colors = colors;
            this.maxPatterns = maxPatterns;
            this.desc = desc;
            this.sizePN = sizePN;
        }
    }
    
    /**
     *
     */
    protected class Cell
    {
        /**
         * Cell number
         */
        public int number;

        /**
         * Cell data
         */
        public short[] data;

        /**
         * original line (y) in the bitmap
         */
        public int lineNb;

        /**
         *
         * @param palsize
         */
        public Cell(int palsize)
        {
            // in 16 colors, 8x8 pixels blocks require 64 bytes to store the 4bits or 8bits palette       
            if (palsize == 16)
            {
                data = new short[64];
            } 
            else if (palsize == 128)
            {
                data = new short[64];
            } 
            else
            {
                data = new short[64];
            }
        }
    }

}
