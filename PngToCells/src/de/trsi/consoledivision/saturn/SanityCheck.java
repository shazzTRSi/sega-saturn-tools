/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.trsi.consoledivision.saturn;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shazz
 */
public class SanityCheck 
{
    private String filename;
    private String prefix;
    
    private int palette[];
    private int page0[];
    private int page1[];
    private long celldata[];    
    private HashMap<Integer, short[]> patterndata;

    public SanityCheck(String filename, String prefix)
    {
        this.filename = filename;
        this.prefix = prefix;
    }
    
    
    public void generatePages()
    {
        try
        {
            int palSize = 0;
            int celldataSize = 0;
            int pageSize = 0;
            String line = null;
            
            // 1. load file
            BufferedReader br = new BufferedReader(new FileReader(this.filename));

            // 1. palette size
            line = br.readLine();            
            palSize = Integer.parseInt(line.substring(line.lastIndexOf("[")+1, line.lastIndexOf("]")));    
            
            palette = new int[palSize];
            
             // 2. palette data                   
            br.readLine(); 
            int idx = 0;
            for(int i=0; i<palSize/16; i++)
            {
                line = br.readLine();
                List<String> entries = Arrays.asList(line.split("\\s*,\\s*"));
                for(String entry: entries)
                {
                    palette[idx++] = Integer.decode(entry.trim());
                }   
            }
            
            br.readLine(); 
            br.readLine(); 
            
            // 3. cell data
            line = br.readLine();            
            celldataSize = Integer.parseInt(line.substring(line.lastIndexOf("[")+1, line.lastIndexOf("]")));   
            celldata = new long[celldataSize];
            
            br.readLine();
            idx = 0;
            for(int i=0; i<celldataSize/16; i++)
            {
                line = br.readLine();
                List<String> entries = Arrays.asList(line.split("\\s*,\\s*"));
                for(String entry: entries)
                {
                    if(!entry.contains("//") && !entry.contains(")"))
                        celldata[idx++] = Long.decode(entry.trim());
                }   
            }
            
            // create cells
            patterndata = new HashMap<>();
            
            idx = 0;
            for(int i=0; i<celldataSize; i+=16)
            {
                short[] aCell = new short[64];
                for(int j=0; j<16; j++)
                {
                    long val = celldata[i+j];
                    if(palSize == 256)
                    {
                        aCell[(4*j)+0] = (short) ((val >> 24) & 0xFF);
                        aCell[(4*j)+1] = (short) ((val >> 16) & 0xFF);
                        aCell[(4*j)+2] = (short) ((val >>  8) & 0xFF);
                        aCell[(4*j)+3] = (short) ((val >>  0) & 0xFF);                    
                    } 
                    else if(palSize == 16)
                    {
                        // TODO
                    }
                }
                patterndata.put(idx, aCell);
                idx++;
            }            
            
            
            br.readLine();
            br.readLine();
            br.readLine();
            br.readLine();
            
            // 4. page0 data
            line = br.readLine();            
            pageSize = Integer.parseInt(line.substring(line.lastIndexOf("[")+1, line.lastIndexOf("]")));   
            page0 = new int[pageSize];
                
            br.readLine();
            idx = 0;
            for(int i=0; i<pageSize/64; i++)
            {
                line = br.readLine();
                List<String> entries = Arrays.asList(line.split("\\s*,\\s*"));
                for(String entry: entries)
                {
                    if(!entry.contains("//") && !entry.contains(")"))
                        page0[idx++] = Integer.decode(entry.trim());
                }   
            }            
            br.readLine();
            br.readLine();
            br.readLine();
            
            // 5. Generate raw bitmap
            generatePageBitmap(0, palSize);
            
            // 6. page0 data
            line = br.readLine();      
            try
            {
                pageSize = Integer.parseInt(line.substring(line.lastIndexOf("[")+1, line.lastIndexOf("]")));   
                page1 = new int[pageSize];

                br.readLine();
                idx = 0;
                for(int i=0; i<pageSize/64; i++)
                {
                    line = br.readLine();
                    List<String> entries = Arrays.asList(line.split("\\s*,\\s*"));
                    for(String entry: entries)
                    {
                        if(!entry.contains("//") && !entry.contains(")"))
                            page1[idx++] = Integer.decode(entry.trim());
                    }   
                }         

                // 7. Generate raw bitmap
                generatePageBitmap(1, palSize);   
            }
            catch(java.lang.NullPointerException ex)
            {
                Logger.getLogger(SanityCheck.class.getName()).log(Level.WARNING, "Only one page found");
            }
                               
            br.close();                       
            
            
        }
        catch(IOException ex)
        {
            System.out.println("Ex: " + ex );
        }
    }

    /*
    * Generate a RAW bitmap, 24bits interleaved RGB     
    */
    private void generatePageBitmap(int page, int colors)
    {
        int bitmap[][] = new int[512][512];
        FileOutputStream fos = null;
        int idx = 0;
        
        int currentPage[] = (page==0)?page0:page1;
        
        try
        {
            fos = new FileOutputStream(this.filename + "." + page + ".raw");  
            
            for(int i=0; i<currentPage.length; i++)
            {
                int offset = currentPage[i]/(this.palette.length >> 7);

                short[] cell = (short[]) patterndata.get(offset);
               
                for(int y=0; y<8; y++)
                {
                    for(int x=0; x<8; x++)
                    {
                        short val = cell[x+(y*8)];
                        int bx = (8*(i % 64))+x;
                        int by = y + 8*(i / 64);
                        bitmap[bx][by] = palette[val] & 0xFFFF;
                    }     
                } 
            }
            
            idx = 0;
            int height = (currentPage.length/64)*8;
            byte data[] = new byte[512*height*3];
            for(int i=0; i<height; i++)
            {
                for(int j=0; j<512; j++)
                {                
                    int pixel = bitmap[j][i] & 0x0FFFF;
                    int r8 = (pixel & 0x1F) << 3;
                    int g8 = ((pixel >>  5) & 0x1F) << 3;
                    int b8 = ((pixel >> 10) & 0x1F) << 3;
                    data[idx++] = (byte) (r8 & 0xFF);
                    data[idx++] = (byte) (g8 & 0xFF);
                    data[idx++] = (byte) (b8 & 0xFF);
                }  
            }
            
            fos.write(data, 0, data.length);
            fos.flush();
            fos.close();
        } 
        catch (IOException ex)
        {
            Logger.getLogger(SanityCheck.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally
        {
            try
            {
                if(fos != null) fos.close();
            } 
            catch (IOException ex)
            {
                Logger.getLogger(SanityCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
   
    
}
