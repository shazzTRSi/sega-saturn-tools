/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.trsi.consoledivision.saturn;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.vecmath.Point2d;

/**
 *
 * @author SC2
 */
public class CurveDraw extends JPanel {

    private int width = 800;
    private int heigth = 400;
    private int padding = 25;
    private int labelPadding = 25;
    private Color lineColor = new Color(44, 102, 230, 180);
    private Color pointColor = new Color(100, 100, 100, 180);
    private Color gridColor = new Color(200, 200, 200, 200);
    private static final Stroke GRAPH_STROKE = new BasicStroke(2f);
    private int pointWidth = 4;
    private int numberYDivisions = 10;
    private List<Point2d> scores;

    public CurveDraw(List<Point2d> scores) {
        this.scores = scores;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        double xScale = ((double) getWidth() - (2 * padding) - labelPadding) / (getMaxScoreX() - getMinScoreX());
        double yScale = ((double) getHeight() - 2 * padding - labelPadding) /  (getMaxScoreY() - getMinScoreY());

        List<Point> graphPoints = new ArrayList<>();
        for (int i = 0; i < scores.size(); i++) 
        {
            int x1;
            if( (int)getMinScoreX() < 0)
                x1 = (int) ( (((Point2d)scores.get(i)).x - (int)getMinScoreX()) * xScale + padding + labelPadding );
            else
                 x1 = (int) ( (((Point2d)scores.get(i)).x) * xScale + padding + labelPadding );
            
            int y1 = (int) ( (getMaxScoreY() - ((Point2d)scores.get(i)).y) * yScale + padding );
            graphPoints.add(new Point(x1, y1));
        }

        // draw white background
        g2.setColor(Color.WHITE);
        g2.fillRect(padding + labelPadding, padding, getWidth() - (2 * padding) - labelPadding, getHeight() - 2 * padding - labelPadding);
        g2.setColor(Color.BLACK);

        // create hatch marks and grid lines for y axis.
        for (int i = 0; i < numberYDivisions + 1; i++) 
        {
            int x0 = padding + labelPadding;
            int x1 = pointWidth + padding + labelPadding;
            int y0 = getHeight() - ((i * (getHeight() - padding * 2 - labelPadding)) / numberYDivisions + padding + labelPadding);
            int y1 = y0;
            if (scores.size() > 0) 
            {
                g2.setColor(gridColor);
                g2.drawLine(padding + labelPadding + 1 + pointWidth, y0, getWidth() - padding, y1);
                g2.setColor(Color.BLACK);
                String yLabel = ((int) ((getMinScoreY() + (getMaxScoreY() - getMinScoreY()) * ((i * 1.0) / numberYDivisions)) * 100)) / 100.0 + "";
                FontMetrics metrics = g2.getFontMetrics();
                int labelWidth = metrics.stringWidth(yLabel);
                g2.drawString(yLabel, x0 - labelWidth - 5, y0 + (metrics.getHeight() / 2) - 3);
            }
            g2.drawLine(x0, y0, x1, y1);
        }

        // and for x axis
        for (int i = (int)getMinScoreX(); i < (int)getMaxScoreX(); i++) 
        {
            if (scores.size() > 1) 
            {
                int x0 = 0;
                if( (int)getMinScoreX() < 0)
                    x0 = (i-(int)getMinScoreX()) * (getWidth() - padding * 2 - labelPadding) / (scores.size() - 1) + padding + labelPadding;
                else
                    x0 = i * (getWidth() - padding * 2 - labelPadding) / (scores.size() - 1) + padding + labelPadding;
                int x1 = x0;
                int y0 = getHeight() - padding - labelPadding;
                int y1 = y0 - pointWidth;
                if ((i % ((int) ((scores.size() / 20.0)) + 1)) == 0) 
                {
                    g2.setColor(gridColor);
                    g2.drawLine(x0, getHeight() - padding - labelPadding - 1 - pointWidth, x1, padding);
                    g2.setColor(Color.BLACK);
                    String xLabel = i + "";
                    FontMetrics metrics = g2.getFontMetrics();
                    int labelWidth = metrics.stringWidth(xLabel);
                    g2.drawString(xLabel, x0 - labelWidth / 2, y0 + metrics.getHeight() + 3);
                }
                g2.drawLine(x0, y0, x1, y1);
            }
        }

        // create x and y axes 
        g2.drawLine(padding + labelPadding, getHeight() - padding - labelPadding, padding + labelPadding, padding);
        g2.drawLine(padding + labelPadding, getHeight() - padding - labelPadding, getWidth() - padding, getHeight() - padding - labelPadding);

        Stroke oldStroke = g2.getStroke();
        g2.setColor(lineColor);
        g2.setStroke(GRAPH_STROKE);
        for (int i = 0; i < graphPoints.size() - 1; i++) 
        {
            int x1 = graphPoints.get(i).x;
            int y1 = graphPoints.get(i).y;
            int x2 = graphPoints.get(i + 1).x;
            int y2 = graphPoints.get(i + 1).y;
            g2.drawLine(x1, y1, x2, y2);
        }

        g2.setStroke(oldStroke);
        g2.setColor(pointColor);
        for (int i = 0; i < graphPoints.size(); i++) 
        {
            int x = graphPoints.get(i).x - pointWidth / 2;
            int y = graphPoints.get(i).y - pointWidth / 2;
            int ovalW = pointWidth;
            int ovalH = pointWidth;
            g2.fillOval(x, y, ovalW, ovalH);
        }
    }

    private double getMinScoreX() {
        double minScore = Double.MAX_VALUE;
        for (Point2d score : scores) {
            minScore = Math.min(minScore, score.x);
        }
        return minScore;
    }

    private double getMaxScoreX() {
        double maxScore = Double.MIN_VALUE;
        for (Point2d score : scores) {
            maxScore = Math.max(maxScore, score.x);
        }
        return maxScore;
    }
    
    private double getMinScoreY() {
        double minScore = Double.MAX_VALUE;
        for (Point2d score : scores) {
            minScore = Math.min(minScore, score.y);
        }
        return minScore;
    }

    private double getMaxScoreY() {
        double maxScore = Double.MIN_VALUE;
        for (Point2d score : scores) {
            maxScore = Math.max(maxScore, score.y);
        }
        return maxScore;
    }

    public void setScores(List<Point2d> scores) {
        this.scores = scores;
        invalidate();
        this.repaint();
    }

    public List<Point2d> getScores() {
        return scores;
    }
    
    private static void createAndShowGui() 
    {
        double A_x = 1;
        double omega_x = 5;
        double A_y = 1;
        double omega_y = 4;
        double delta_x = 0;
        double delta_y = 0;
                
        List<Point2d> scores = new ArrayList<>();
        int maxDataPoints = 20;
        for (int t = 0; t < maxDataPoints; t++) 
        {
            scores.add(new Point2d(A_x*Math.sin(omega_x*t+delta_x) , A_y*Math.sin(omega_y*t+delta_y)));
        }
        CurveDraw mainPanel = new CurveDraw(scores);
        mainPanel.setPreferredSize(new Dimension(800, 600));
        JFrame frame = new JFrame("DrawGraph");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            createAndShowGui();
         }
      });    }
    
}
