/*
 * PNG to Saturn Sprite converter
 * 
 * Extract pixel data and convert them to Saturn BGR15 format with support for
 * Transparency and End Code (ECD)
 *
 * (C) 2015 - Shazz/TRSi
 */
package de.trsi.consoledivision.saturn;

import ar.com.hjg.pngj.IImageLineSet;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.util.Arrays.asList;
import java.util.logging.Level;
import java.util.logging.Logger;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 *
 * @author Shazz
 */
public class PngToSprite {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {

        PngToSprite pngToTable = new PngToSprite();

        OptionParser p = new OptionParser();
        p.acceptsAll(asList( "h", "?" ), "show this help" ).forHelp();

        p.acceptsAll(asList( "input", "i"), "input file full path").withRequiredArg().required();
        p.acceptsAll(asList( "output", "o"), "output file full path").withRequiredArg().required();

        OptionSet opt = p.parse(args);
        String input = (String) opt.valueOf("input");
        String output = (String) opt.valueOf("output");

        pngToTable.extract(input, output);      
    }
    
    /**
     * Extract 
     * @param input
     * @param output
     */
    public void extract(String input, String output)
    {
        PngReader pngr = null;
        BufferedWriter bufferedWriter = null;;
        
        try 
        {

            Path path = Paths.get(input);
            String filename = path.getFileName().toString().substring(0, path.getFileName().toString().lastIndexOf('.')).replace(' ', '_');

            byte[] data = Files.readAllBytes(path);
            
            InputStream is = new ByteArrayInputStream(data);
            
            pngr = new PngReader(is, true);
            Charset charset = Charset.forName("UTF-8");
            bufferedWriter = Files.newBufferedWriter(Paths.get(output), charset);
            StringBuilder sb = new StringBuilder();
            
            int height = pngr.getImgInfo().rows;
            int width = pngr.getImgInfo().cols;
            
            // check PNG is valid for our purposes : 32bits with alpha channel, width mutiploe of 8
            if(width % 8 != 0) throw new PngToSpriteException("Width should be multiple of 8 pixels");
            if(pngr.getMetadata().getPLTE() != null) throw new PngToSpriteException("Should be a 32bits png");
            if(!pngr.getImgInfo().alpha) throw new PngToSpriteException("Should be a 32bits png with alpha channel");
            
            IImageLineSet<ImageLineInt> scanlines = (IImageLineSet<ImageLineInt>) pngr.readRows();
            
            sb.append("const uint16_t " + filename + "_char_pat[").append(height*width).append("] = {\n\t//sprite colors in BGR15\n\t");
            
            int tab = 0;
            for (int y=0; y<height; y++) 
            {
                ImageLineInt aScanline = (ImageLineInt) scanlines.getImageLine(y);
                
                for (int x=0; x<width; x++) 
                {                
                    // each elem is one component of the 32bits pixel: r,g,b,a
                    int r8 = aScanline.getElem((4*x)+0);
                    int g8 = aScanline.getElem((4*x)+1);
                    int b8 = aScanline.getElem((4*x)+2);
                    int a8 = aScanline.getElem((4*x)+3);

                    if(a8 == 0) // check fully transparent pixel
                    {
                        sb.append(toHexString( 0x0, 4));
                        //Logger.getLogger(PngToSprite.class.getName()).log(Level.INFO, null, "pix(" + x + "," + y + ") is transparent");
                    }
                    else if(a8 == 128) // check ECD
                    {
                        sb.append(toHexString( 0x7FFF, 4));
                        //Logger.getLogger(PngToSprite.class.getName()).log(Level.INFO, null, "pix(" + x + "," + y + ") is ECD");
                    }                      
                    else
                    {
                        // remember to set bit 16 to indicate this is a BGR pixel
                        int r5 = (r8 >> 3) & 0x1F;
                        int g5 = (g8 >> 3) & 0x1F;
                        int b5 = (b8 >> 3) & 0x1F;     
                        sb.append(toHexString( (((b5 << 10) | (g5 <<5) | r5) & 0x7FFF) | 0x8000, 4));
                        //System.out.println("pix(" + x + "," + y + ") " + y + " R " + r8 + " G " + g8 + " B " + b8 + " => R5 " + r5 + " G " + g5 + " B " + b5 + " == " + toHexString(((b5 << 10) | (g5 <<5) | r5) & 0x7FFF, 4));
                    }
                    sb.append(", ");
                    if(++tab % 16 == 0) sb.append("\n\t");  
                }
            }
            sb.append("\n};\n\n"); 
            bufferedWriter.write(sb.toString(), 0, sb.toString().length());
            bufferedWriter.close();
            
            pngr.close();
            
        } 
        catch (IOException | PngToSpriteException ex) 
        {
            Logger.getLogger(PngToSprite.class.getName()).log(Level.SEVERE, null, ex);    
            if(pngr != null) pngr.close();               
        }

    }
    
    private String toHexString(int entry, int size)
    {
        return "0x" + String.format("%0" + size + "x", entry).toUpperCase();
    }
}
