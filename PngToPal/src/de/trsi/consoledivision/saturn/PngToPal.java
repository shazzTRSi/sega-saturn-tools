/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.trsi.consoledivision.saturn;

import ar.com.hjg.pngj.IImageLineSet;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.chunks.PngChunkPLTE;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.util.Arrays.asList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 *
 * @author SC2
 */
public class PngToPal
{
    private final HashMap<Integer, Integer> palette;
    private IImageLineSet<ImageLineInt> scanlines;
    private int palsize;
    private int width;
    private int height;
    private String prefix;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            PngToPal pngToPal = new PngToPal();

            OptionParser p = new OptionParser();
            p.allowsUnrecognizedOptions();
            p.acceptsAll(asList("h", "?"), "show this help").forHelp();

            p.acceptsAll(asList("input", "i"), "input file full path").withRequiredArg().required();
            p.acceptsAll(asList("output", "o"), "output file full path").withRequiredArg().required();

            OptionSet opt = p.parse(args);
            String input = (String) opt.valueOf("input");
            String output = (String) opt.valueOf("output");

            pngToPal.process(input, output);      
        } 
        catch (PngToPalException | IOException ex)
        {
            Logger.getLogger(PngToPal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Constructor
     */
    public PngToPal()
    {
        this.palette = new HashMap();
        this.palsize = 0;                  
    }

    /**
     *
     * @param input
     * @param output
     * @param format
     * @throws pngToPalException
     * @throws IOException
     */
    public void process(String input, String output) throws PngToPalException, IOException
    {
        BufferedWriter bufferedWriter = null;
        InputStream is = null;

        try
        {
            int nbBlocks = 0;
            int nbBlocksPerLine = 0;            
          
            
            Path path_in = Paths.get(input);
            if(!path_in.toFile().exists()) throw new PngToPalException("Input file missing : " + path_in);
                
            Path path_out = Paths.get(output);
            this.prefix = path_out.getFileName().toString().substring(0, path_out.getFileName().toString().lastIndexOf('.')).replace(' ', '_');

            byte[] data = Files.readAllBytes(path_in);
            is = new ByteArrayInputStream(data);

            bufferedWriter = Files.newBufferedWriter(Paths.get(output), Charset.forName("UTF-8"));

            PngReader pngr = new PngReader(is, true);
            
            if (!pngr.getImgInfo().indexed) throw new PngToPalException("Not a indexed PNG");

            this.height = pngr.getImgInfo().rows;
            this.width = pngr.getImgInfo().cols;       
            
            // Extract palette
            String palBuf = extractPalette(pngr.getMetadata().getPLTE());
            bufferedWriter.write(palBuf, 0, palBuf.length());

           
            is.close();
            pngr.close();
            bufferedWriter.close();
        } 
        catch (IOException ex)
        {
            Logger.getLogger(PngToPal.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally
        {
            try
            {
                if (bufferedWriter != null)
                {
                    bufferedWriter.close();
                }
                if (is != null)
                {
                    is.close();
                }
            } catch (IOException ex)
            {
                throw new PngToPalException(ex.getMessage());
            }
        }

    }

    /*
     * Not perfect linear RGB5 to BGR5 conversion. 
     */
    private int rgb8tobgr5(int rgb8)
    {
        int r8 = (rgb8 >> 16) & 0xFF;
        int g8 = (rgb8 >> 8) & 0xFF;
        int b8 = rgb8 & 0xFF;

        int r5 = (r8 >> 3) & 0x1F;
        int g5 = (g8 >> 3) & 0x1F;
        int b5 = (b8 >> 3) & 0x1F;

        int bgr5 = ((b5 << 10) | (g5 << 5) | r5) & 0x7FFF;
        return bgr5;

    }
   

    private String extractPalette(PngChunkPLTE pal) throws PngToPalException
    {
        StringBuilder sb = new StringBuilder();

        if (pal.getNentries() <= 16)
        {
            this.palsize = 16;
            sb.append("const uint16_t " + this.prefix + "_cell_palette[").append(16).append("] __attribute__((aligned(4))) = {\n\t//16 colors palette in BGR15 format (not RGB)\n\t");
            for (int i = 0; i < pal.getNentries(); i++)
            {
                int bgr5 = rgb8tobgr5(pal.getEntry(i));

                this.palette.put(i, bgr5);
                sb.append(toHexString(bgr5, 4));
                if (i != 16 - 1)
                {
                    sb.append(", ");
                }
            }
            sb.append("\n};\n\n");
            
            return sb.toString();
        } 
        else if (pal.getNentries() == 256)
        {
            int tab = 0;
            this.palsize = 256;
            sb.append("const uint16_t " + this.prefix + "_cell_palette[").append(pal.getNentries()).append("] __attribute__((aligned(4))) = {\n\t//256 colors palette in BGR15 format (not RGB)\n\t");
            for (int i = 0; i < pal.getNentries(); i++)
            {
                tab++;
                int bgr5 = rgb8tobgr5(pal.getEntry(i));

                this.palette.put(i, bgr5);
                sb.append(toHexString(bgr5, 4));

                if (i != 256 - 1)
                {
                    if (tab % 16 == 0)
                    {
                        sb.append(",\n\t");
                    } else
                    {
                        sb.append(", ");
                    }
                }
            }
            sb.append("\n};\n\n");
            
            return sb.toString();
        } 
        else
        {
            throw new PngToPalException("Palette is too big : " + pal.getNentries());
        }
    }

    private String toHexString(int entry, int size)
    {
        return "0x" + String.format("%0" + size + "x", entry).toUpperCase();
    } 


}
